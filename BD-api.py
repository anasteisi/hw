from app import create_app, db
from app.models import BD_point

app = create_app()


@app.shell_context_processor
def make_shell_context():
    return {'db': db, 'BD_point': BD_point}
